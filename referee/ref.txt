1.  Åtgärdat gm. att införa ordet "impurity" där det saknades. Påpeka
    distinktionen i svaret.

2.  Förklara att den inte är linjär i svaret. Krävs någon kommentar i texten?
    Förtydligande om modellen; kanske stryka nästan hela 2.1?

3.  Ja du, jag antar att vi får kolla på de där artiklarna och kanske referera
    till dem om det är något att ta upp?

4.  Nej, det är sant. Och det bör vi ju nämna att vi inte gör, både i början
    och i slutet. Hur är det med roto och GENE.

5.  Jo, det är sant. Jag har tagit bor den dåliga punkten och ändrat i
    bildtexten. Jag vet inte riktigt hur det ska svaras till Refereen dock.

6.  Hela GENE-delen är ganska vag. Jag vet inte riktigt hur den ska kunna
    stärkas? Kanske kolla på nån nyare Jenko-artikel och se hur de introducerar
    den? 
    Poloidal is now binormal, following Merz. Still vague though.

7.  Nej, kollisioner är inte med. Föreslår att vi påpekar det, och lutar oss
    mot Nordman 2011 som stöd för att effekten antagligen är liten.

8.  Upplösningen har kollats genom att öka och misnka den, och se vad effekten
    blev på spektran och flödesnivåer. Hur detaljerat behöver vi skriva i
    texten, respektive i artikeln, dock?
    EDIT: Har lagt till ett par meningar om detta; se manus.

9.  Men har effekterna på impurities studerats? Måste kolla det igen,
    noggrannare.

10. Jag har bytt ut bilden och ändrat i texten till något mindre dramatiskt.
    Räcker så?
  
11. Här är en språkförbistring. Motivera användandet av PF gentemot källor som
    använder det som vi i artikeln (förslag?) samt (speciellt!) i svaret.

12. Språkförbistring igen, dock av det svengelska slaget. Jag har åtgärdat det,
    men är inte helt nöjd med ordvalet.

13. Jag tror jag har åtgärdat referensen, men resten vet jag itne riktigt var
    det bör avhandlas.

14. Så vitt jag kan se står det tydligt i figurtexten... Hur säger man det på
    ett snällt sätt?

15. Återigen, rör det sig verkligen om impurities? Detta rör hursomhelst inte
    denna artikel så mycket, men det kan ju vara värt att formulera om
    outlooken lite ändå?

De generella (NB: och viktigare!) kommentarerna:
    "models presented i section 2" -- det är ju sant att dessa är gamla.
        Betona mer eller stryka? Jag tror inte det refereras tillbaka till dem
        så mycket i texten egentligen. (Se även 2.)
    "main results claimed" -- det bör ju kollas noggrannare, men om vi kan
        försäkra oss om att impurityaspekten är ny, bör det ju bara vara att
        påpeka?  Kan det förtydligas ännu mer, att det är impurities det
        handlar om utan att det blir löjligt övertydligt? (Se 1.)
    "mention of any previous relevant results" -- kan vi göra det bättre?
    "discussion if the limitations" -- Detta kan nog också göras bättre... (Se
        4., 6.--7.)
    


